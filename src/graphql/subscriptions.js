/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateGym = /* GraphQL */ `
  subscription OnCreateGym {
    onCreateGym {
      id
      name
      location
      businessHours {
        startTime
        endTime
      }
      pictures
      studio {
        items {
          id
          name
          gymID
          gym {
            id
            name
            location
            pictures
            createdAt
            updatedAt
          }
          devices {
            nextToken
          }
          sessions {
            nextToken
          }
          customizations {
            temperature
            light
          }
          pictures
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateGym = /* GraphQL */ `
  subscription OnUpdateGym {
    onUpdateGym {
      id
      name
      location
      businessHours {
        startTime
        endTime
      }
      pictures
      studio {
        items {
          id
          name
          gymID
          gym {
            id
            name
            location
            pictures
            createdAt
            updatedAt
          }
          devices {
            nextToken
          }
          sessions {
            nextToken
          }
          customizations {
            temperature
            light
          }
          pictures
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteGym = /* GraphQL */ `
  subscription OnDeleteGym {
    onDeleteGym {
      id
      name
      location
      businessHours {
        startTime
        endTime
      }
      pictures
      studio {
        items {
          id
          name
          gymID
          gym {
            id
            name
            location
            pictures
            createdAt
            updatedAt
          }
          devices {
            nextToken
          }
          sessions {
            nextToken
          }
          customizations {
            temperature
            light
          }
          pictures
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateStudio = /* GraphQL */ `
  subscription OnCreateStudio {
    onCreateStudio {
      id
      name
      gymID
      gym {
        id
        name
        location
        businessHours {
          startTime
          endTime
        }
        pictures
        studio {
          items {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      devices {
        items {
          id
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      sessions {
        items {
          id
          title
          isCanceled
          cancellationReason
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          zoomMeetID
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          startTime
          endTime
          rating
          notes
          overtimePenalty
          createdAt
          updatedAt
        }
        nextToken
      }
      customizations {
        temperature
        light
      }
      pictures
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateStudio = /* GraphQL */ `
  subscription OnUpdateStudio {
    onUpdateStudio {
      id
      name
      gymID
      gym {
        id
        name
        location
        businessHours {
          startTime
          endTime
        }
        pictures
        studio {
          items {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      devices {
        items {
          id
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      sessions {
        items {
          id
          title
          isCanceled
          cancellationReason
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          zoomMeetID
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          startTime
          endTime
          rating
          notes
          overtimePenalty
          createdAt
          updatedAt
        }
        nextToken
      }
      customizations {
        temperature
        light
      }
      pictures
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteStudio = /* GraphQL */ `
  subscription OnDeleteStudio {
    onDeleteStudio {
      id
      name
      gymID
      gym {
        id
        name
        location
        businessHours {
          startTime
          endTime
        }
        pictures
        studio {
          items {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      devices {
        items {
          id
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      sessions {
        items {
          id
          title
          isCanceled
          cancellationReason
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          zoomMeetID
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          startTime
          endTime
          rating
          notes
          overtimePenalty
          createdAt
          updatedAt
        }
        nextToken
      }
      customizations {
        temperature
        light
      }
      pictures
      createdAt
      updatedAt
    }
  }
`;
export const onCreateDevice = /* GraphQL */ `
  subscription OnCreateDevice {
    onCreateDevice {
      id
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateDevice = /* GraphQL */ `
  subscription OnUpdateDevice {
    onUpdateDevice {
      id
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteDevice = /* GraphQL */ `
  subscription OnDeleteDevice {
    onDeleteDevice {
      id
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateSession = /* GraphQL */ `
  subscription OnCreateSession {
    onCreateSession {
      id
      title
      isCanceled
      cancellationReason
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      zoomMeetID
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      startTime
      endTime
      rating
      notes
      overtimePenalty
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateSession = /* GraphQL */ `
  subscription OnUpdateSession {
    onUpdateSession {
      id
      title
      isCanceled
      cancellationReason
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      zoomMeetID
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      startTime
      endTime
      rating
      notes
      overtimePenalty
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteSession = /* GraphQL */ `
  subscription OnDeleteSession {
    onDeleteSession {
      id
      title
      isCanceled
      cancellationReason
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      zoomMeetID
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      startTime
      endTime
      rating
      notes
      overtimePenalty
      createdAt
      updatedAt
    }
  }
`;
export const onCreateUser = /* GraphQL */ `
  subscription OnCreateUser {
    onCreateUser {
      id
      username
      email
      userGroup
      intakeFormDetails {
        personalDetails {
          fullName
          ageGroup
          height
          weight
          sex
          phoneNumber
          email
          emergencyContactName
          emergencyContactNumber
        }
        exerciseHistory {
          exerciseImportance
          weeklyExercise
          ageOfMostConsistency
          athleticismLevel
          exercisePrograms
          wantedExerciseAchievements
          confidenceToAchieveGoals
        }
        medicalHistory {
          hasMedicalConditions {
            isQuestionTrue
            reason
          }
          toldToLimitActivity {
            isQuestionTrue
            reason
          }
          injuries {
            isQuestionTrue
            reason
          }
          surgeries {
            isQuestionTrue
            reason
          }
          chronicDisease {
            isQuestionTrue
            reason
          }
          medications {
            isQuestionTrue
            reason
          }
        }
        behavioralQuestions {
          barrierToExercise
          confidenceToOvercomeBarrier
          friendAndFamilySupport
          exerciseImportance
          motivation
          resultsExpectation
        }
        trainerPreferences {
          language
          trainerSex
          coachPersonality
        }
      }
      profileInformation {
        firstName
        lastName
        emailAddress
        contactNumber
        coachRate
        cancellationRate
        about
        languages
        skills
        coachSchedule {
          scheduleExceptions {
            id
            type
            day
            coachRate
            startTime
            endTime
          }
        }
        timezone
        numOfVirtualClients
        socialMediaLinks
        profilePicture
        certifications
      }
      currentMembershipOption {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      pastMembershipOptions {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      averageRating
      coachReviews {
        id
        clientID
        client {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        coachID
        coach {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        title
        rating
        reviewText
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateUser = /* GraphQL */ `
  subscription OnUpdateUser {
    onUpdateUser {
      id
      username
      email
      userGroup
      intakeFormDetails {
        personalDetails {
          fullName
          ageGroup
          height
          weight
          sex
          phoneNumber
          email
          emergencyContactName
          emergencyContactNumber
        }
        exerciseHistory {
          exerciseImportance
          weeklyExercise
          ageOfMostConsistency
          athleticismLevel
          exercisePrograms
          wantedExerciseAchievements
          confidenceToAchieveGoals
        }
        medicalHistory {
          hasMedicalConditions {
            isQuestionTrue
            reason
          }
          toldToLimitActivity {
            isQuestionTrue
            reason
          }
          injuries {
            isQuestionTrue
            reason
          }
          surgeries {
            isQuestionTrue
            reason
          }
          chronicDisease {
            isQuestionTrue
            reason
          }
          medications {
            isQuestionTrue
            reason
          }
        }
        behavioralQuestions {
          barrierToExercise
          confidenceToOvercomeBarrier
          friendAndFamilySupport
          exerciseImportance
          motivation
          resultsExpectation
        }
        trainerPreferences {
          language
          trainerSex
          coachPersonality
        }
      }
      profileInformation {
        firstName
        lastName
        emailAddress
        contactNumber
        coachRate
        cancellationRate
        about
        languages
        skills
        coachSchedule {
          scheduleExceptions {
            id
            type
            day
            coachRate
            startTime
            endTime
          }
        }
        timezone
        numOfVirtualClients
        socialMediaLinks
        profilePicture
        certifications
      }
      currentMembershipOption {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      pastMembershipOptions {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      averageRating
      coachReviews {
        id
        clientID
        client {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        coachID
        coach {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        title
        rating
        reviewText
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteUser = /* GraphQL */ `
  subscription OnDeleteUser {
    onDeleteUser {
      id
      username
      email
      userGroup
      intakeFormDetails {
        personalDetails {
          fullName
          ageGroup
          height
          weight
          sex
          phoneNumber
          email
          emergencyContactName
          emergencyContactNumber
        }
        exerciseHistory {
          exerciseImportance
          weeklyExercise
          ageOfMostConsistency
          athleticismLevel
          exercisePrograms
          wantedExerciseAchievements
          confidenceToAchieveGoals
        }
        medicalHistory {
          hasMedicalConditions {
            isQuestionTrue
            reason
          }
          toldToLimitActivity {
            isQuestionTrue
            reason
          }
          injuries {
            isQuestionTrue
            reason
          }
          surgeries {
            isQuestionTrue
            reason
          }
          chronicDisease {
            isQuestionTrue
            reason
          }
          medications {
            isQuestionTrue
            reason
          }
        }
        behavioralQuestions {
          barrierToExercise
          confidenceToOvercomeBarrier
          friendAndFamilySupport
          exerciseImportance
          motivation
          resultsExpectation
        }
        trainerPreferences {
          language
          trainerSex
          coachPersonality
        }
      }
      profileInformation {
        firstName
        lastName
        emailAddress
        contactNumber
        coachRate
        cancellationRate
        about
        languages
        skills
        coachSchedule {
          scheduleExceptions {
            id
            type
            day
            coachRate
            startTime
            endTime
          }
        }
        timezone
        numOfVirtualClients
        socialMediaLinks
        profilePicture
        certifications
      }
      currentMembershipOption {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      pastMembershipOptions {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      averageRating
      coachReviews {
        id
        clientID
        client {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        coachID
        coach {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        title
        rating
        reviewText
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const onCreateReview = /* GraphQL */ `
  subscription OnCreateReview {
    onCreateReview {
      id
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      title
      rating
      reviewText
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateReview = /* GraphQL */ `
  subscription OnUpdateReview {
    onUpdateReview {
      id
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      title
      rating
      reviewText
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteReview = /* GraphQL */ `
  subscription OnDeleteReview {
    onDeleteReview {
      id
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      title
      rating
      reviewText
      createdAt
      updatedAt
    }
  }
`;
export const onCreateZoomMeet = /* GraphQL */ `
  subscription OnCreateZoomMeet {
    onCreateZoomMeet {
      id
      start_url
      topic
      join_url
      password
      start_time
      duration
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateZoomMeet = /* GraphQL */ `
  subscription OnUpdateZoomMeet {
    onUpdateZoomMeet {
      id
      start_url
      topic
      join_url
      password
      start_time
      duration
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteZoomMeet = /* GraphQL */ `
  subscription OnDeleteZoomMeet {
    onDeleteZoomMeet {
      id
      start_url
      topic
      join_url
      password
      start_time
      duration
      createdAt
      updatedAt
    }
  }
`;
