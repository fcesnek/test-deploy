/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getGym = /* GraphQL */ `
  query GetGym($id: ID!) {
    getGym(id: $id) {
      id
      name
      location
      businessHours {
        startTime
        endTime
      }
      pictures
      studio {
        items {
          id
          name
          gymID
          gym {
            id
            name
            location
            pictures
            createdAt
            updatedAt
          }
          devices {
            nextToken
          }
          sessions {
            nextToken
          }
          customizations {
            temperature
            light
          }
          pictures
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const listGyms = /* GraphQL */ `
  query ListGyms(
    $filter: ModelGymFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listGyms(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        location
        businessHours {
          startTime
          endTime
        }
        pictures
        studio {
          items {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getStudio = /* GraphQL */ `
  query GetStudio($id: ID!) {
    getStudio(id: $id) {
      id
      name
      gymID
      gym {
        id
        name
        location
        businessHours {
          startTime
          endTime
        }
        pictures
        studio {
          items {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      devices {
        items {
          id
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      sessions {
        items {
          id
          title
          isCanceled
          cancellationReason
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          zoomMeetID
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          startTime
          endTime
          rating
          notes
          overtimePenalty
          createdAt
          updatedAt
        }
        nextToken
      }
      customizations {
        temperature
        light
      }
      pictures
      createdAt
      updatedAt
    }
  }
`;
export const listStudios = /* GraphQL */ `
  query ListStudios(
    $filter: ModelStudioFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listStudios(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getDevice = /* GraphQL */ `
  query GetDevice($id: ID!) {
    getDevice(id: $id) {
      id
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listDevices = /* GraphQL */ `
  query ListDevices(
    $filter: ModelDeviceFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listDevices(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        studioID
        studio {
          id
          name
          gymID
          gym {
            id
            name
            location
            pictures
            createdAt
            updatedAt
          }
          devices {
            nextToken
          }
          sessions {
            nextToken
          }
          customizations {
            temperature
            light
          }
          pictures
          createdAt
          updatedAt
        }
        zoomMeet {
          id
          start_url
          topic
          join_url
          password
          start_time
          duration
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getSession = /* GraphQL */ `
  query GetSession($id: ID!) {
    getSession(id: $id) {
      id
      title
      isCanceled
      cancellationReason
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      zoomMeetID
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      startTime
      endTime
      rating
      notes
      overtimePenalty
      createdAt
      updatedAt
    }
  }
`;
export const listSessions = /* GraphQL */ `
  query ListSessions(
    $filter: ModelSessionFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listSessions(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        title
        isCanceled
        cancellationReason
        studioID
        studio {
          id
          name
          gymID
          gym {
            id
            name
            location
            pictures
            createdAt
            updatedAt
          }
          devices {
            nextToken
          }
          sessions {
            nextToken
          }
          customizations {
            temperature
            light
          }
          pictures
          createdAt
          updatedAt
        }
        coachID
        coach {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        clientID
        client {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        zoomMeetID
        zoomMeet {
          id
          start_url
          topic
          join_url
          password
          start_time
          duration
          createdAt
          updatedAt
        }
        startTime
        endTime
        rating
        notes
        overtimePenalty
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getUser = /* GraphQL */ `
  query GetUser($id: ID!) {
    getUser(id: $id) {
      id
      username
      email
      userGroup
      intakeFormDetails {
        personalDetails {
          fullName
          ageGroup
          height
          weight
          sex
          phoneNumber
          email
          emergencyContactName
          emergencyContactNumber
        }
        exerciseHistory {
          exerciseImportance
          weeklyExercise
          ageOfMostConsistency
          athleticismLevel
          exercisePrograms
          wantedExerciseAchievements
          confidenceToAchieveGoals
        }
        medicalHistory {
          hasMedicalConditions {
            isQuestionTrue
            reason
          }
          toldToLimitActivity {
            isQuestionTrue
            reason
          }
          injuries {
            isQuestionTrue
            reason
          }
          surgeries {
            isQuestionTrue
            reason
          }
          chronicDisease {
            isQuestionTrue
            reason
          }
          medications {
            isQuestionTrue
            reason
          }
        }
        behavioralQuestions {
          barrierToExercise
          confidenceToOvercomeBarrier
          friendAndFamilySupport
          exerciseImportance
          motivation
          resultsExpectation
        }
        trainerPreferences {
          language
          trainerSex
          coachPersonality
        }
      }
      profileInformation {
        firstName
        lastName
        emailAddress
        contactNumber
        coachRate
        cancellationRate
        about
        languages
        skills
        coachSchedule {
          scheduleExceptions {
            id
            type
            day
            coachRate
            startTime
            endTime
          }
        }
        timezone
        numOfVirtualClients
        socialMediaLinks
        profilePicture
        certifications
      }
      currentMembershipOption {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      pastMembershipOptions {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      averageRating
      coachReviews {
        id
        clientID
        client {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        coachID
        coach {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        title
        rating
        reviewText
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const listUsers = /* GraphQL */ `
  query ListUsers(
    $filter: ModelUserFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listUsers(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getReview = /* GraphQL */ `
  query GetReview($id: ID!) {
    getReview(id: $id) {
      id
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      title
      rating
      reviewText
      createdAt
      updatedAt
    }
  }
`;
export const listReviews = /* GraphQL */ `
  query ListReviews(
    $filter: ModelReviewFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listReviews(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        clientID
        client {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        coachID
        coach {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        title
        rating
        reviewText
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
export const getZoomMeet = /* GraphQL */ `
  query GetZoomMeet($id: ID!) {
    getZoomMeet(id: $id) {
      id
      start_url
      topic
      join_url
      password
      start_time
      duration
      createdAt
      updatedAt
    }
  }
`;
export const listZoomMeets = /* GraphQL */ `
  query ListZoomMeets(
    $filter: ModelZoomMeetFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listZoomMeets(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      nextToken
    }
  }
`;
