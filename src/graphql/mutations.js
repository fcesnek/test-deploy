/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createZoomMeetApi = /* GraphQL */ `
  mutation CreateZoomMeetApi(
    $topic: String
    $start_time: AWSDateTime
    $duration: Int
  ) {
    createZoomMeetApi(
      topic: $topic
      start_time: $start_time
      duration: $duration
    ) {
      id
      start_url
      topic
      join_url
      password
      start_time
      duration
      createdAt
      updatedAt
    }
  }
`;
export const createGym = /* GraphQL */ `
  mutation CreateGym(
    $input: CreateGymInput!
    $condition: ModelGymConditionInput
  ) {
    createGym(input: $input, condition: $condition) {
      id
      name
      location
      businessHours {
        startTime
        endTime
      }
      pictures
      studio {
        items {
          id
          name
          gymID
          gym {
            id
            name
            location
            pictures
            createdAt
            updatedAt
          }
          devices {
            nextToken
          }
          sessions {
            nextToken
          }
          customizations {
            temperature
            light
          }
          pictures
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateGym = /* GraphQL */ `
  mutation UpdateGym(
    $input: UpdateGymInput!
    $condition: ModelGymConditionInput
  ) {
    updateGym(input: $input, condition: $condition) {
      id
      name
      location
      businessHours {
        startTime
        endTime
      }
      pictures
      studio {
        items {
          id
          name
          gymID
          gym {
            id
            name
            location
            pictures
            createdAt
            updatedAt
          }
          devices {
            nextToken
          }
          sessions {
            nextToken
          }
          customizations {
            temperature
            light
          }
          pictures
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteGym = /* GraphQL */ `
  mutation DeleteGym(
    $input: DeleteGymInput!
    $condition: ModelGymConditionInput
  ) {
    deleteGym(input: $input, condition: $condition) {
      id
      name
      location
      businessHours {
        startTime
        endTime
      }
      pictures
      studio {
        items {
          id
          name
          gymID
          gym {
            id
            name
            location
            pictures
            createdAt
            updatedAt
          }
          devices {
            nextToken
          }
          sessions {
            nextToken
          }
          customizations {
            temperature
            light
          }
          pictures
          createdAt
          updatedAt
        }
        nextToken
      }
      createdAt
      updatedAt
    }
  }
`;
export const createStudio = /* GraphQL */ `
  mutation CreateStudio(
    $input: CreateStudioInput!
    $condition: ModelStudioConditionInput
  ) {
    createStudio(input: $input, condition: $condition) {
      id
      name
      gymID
      gym {
        id
        name
        location
        businessHours {
          startTime
          endTime
        }
        pictures
        studio {
          items {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      devices {
        items {
          id
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      sessions {
        items {
          id
          title
          isCanceled
          cancellationReason
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          zoomMeetID
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          startTime
          endTime
          rating
          notes
          overtimePenalty
          createdAt
          updatedAt
        }
        nextToken
      }
      customizations {
        temperature
        light
      }
      pictures
      createdAt
      updatedAt
    }
  }
`;
export const updateStudio = /* GraphQL */ `
  mutation UpdateStudio(
    $input: UpdateStudioInput!
    $condition: ModelStudioConditionInput
  ) {
    updateStudio(input: $input, condition: $condition) {
      id
      name
      gymID
      gym {
        id
        name
        location
        businessHours {
          startTime
          endTime
        }
        pictures
        studio {
          items {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      devices {
        items {
          id
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      sessions {
        items {
          id
          title
          isCanceled
          cancellationReason
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          zoomMeetID
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          startTime
          endTime
          rating
          notes
          overtimePenalty
          createdAt
          updatedAt
        }
        nextToken
      }
      customizations {
        temperature
        light
      }
      pictures
      createdAt
      updatedAt
    }
  }
`;
export const deleteStudio = /* GraphQL */ `
  mutation DeleteStudio(
    $input: DeleteStudioInput!
    $condition: ModelStudioConditionInput
  ) {
    deleteStudio(input: $input, condition: $condition) {
      id
      name
      gymID
      gym {
        id
        name
        location
        businessHours {
          startTime
          endTime
        }
        pictures
        studio {
          items {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          nextToken
        }
        createdAt
        updatedAt
      }
      devices {
        items {
          id
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        nextToken
      }
      sessions {
        items {
          id
          title
          isCanceled
          cancellationReason
          studioID
          studio {
            id
            name
            gymID
            pictures
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          zoomMeetID
          zoomMeet {
            id
            start_url
            topic
            join_url
            password
            start_time
            duration
            createdAt
            updatedAt
          }
          startTime
          endTime
          rating
          notes
          overtimePenalty
          createdAt
          updatedAt
        }
        nextToken
      }
      customizations {
        temperature
        light
      }
      pictures
      createdAt
      updatedAt
    }
  }
`;
export const createDevice = /* GraphQL */ `
  mutation CreateDevice(
    $input: CreateDeviceInput!
    $condition: ModelDeviceConditionInput
  ) {
    createDevice(input: $input, condition: $condition) {
      id
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateDevice = /* GraphQL */ `
  mutation UpdateDevice(
    $input: UpdateDeviceInput!
    $condition: ModelDeviceConditionInput
  ) {
    updateDevice(input: $input, condition: $condition) {
      id
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteDevice = /* GraphQL */ `
  mutation DeleteDevice(
    $input: DeleteDeviceInput!
    $condition: ModelDeviceConditionInput
  ) {
    deleteDevice(input: $input, condition: $condition) {
      id
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createSession = /* GraphQL */ `
  mutation CreateSession(
    $input: CreateSessionInput!
    $condition: ModelSessionConditionInput
  ) {
    createSession(input: $input, condition: $condition) {
      id
      title
      isCanceled
      cancellationReason
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      zoomMeetID
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      startTime
      endTime
      rating
      notes
      overtimePenalty
      createdAt
      updatedAt
    }
  }
`;
export const updateSession = /* GraphQL */ `
  mutation UpdateSession(
    $input: UpdateSessionInput!
    $condition: ModelSessionConditionInput
  ) {
    updateSession(input: $input, condition: $condition) {
      id
      title
      isCanceled
      cancellationReason
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      zoomMeetID
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      startTime
      endTime
      rating
      notes
      overtimePenalty
      createdAt
      updatedAt
    }
  }
`;
export const deleteSession = /* GraphQL */ `
  mutation DeleteSession(
    $input: DeleteSessionInput!
    $condition: ModelSessionConditionInput
  ) {
    deleteSession(input: $input, condition: $condition) {
      id
      title
      isCanceled
      cancellationReason
      studioID
      studio {
        id
        name
        gymID
        gym {
          id
          name
          location
          businessHours {
            startTime
            endTime
          }
          pictures
          studio {
            nextToken
          }
          createdAt
          updatedAt
        }
        devices {
          items {
            id
            studioID
            createdAt
            updatedAt
          }
          nextToken
        }
        sessions {
          items {
            id
            title
            isCanceled
            cancellationReason
            studioID
            coachID
            clientID
            zoomMeetID
            startTime
            endTime
            rating
            notes
            overtimePenalty
            createdAt
            updatedAt
          }
          nextToken
        }
        customizations {
          temperature
          light
        }
        pictures
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      zoomMeetID
      zoomMeet {
        id
        start_url
        topic
        join_url
        password
        start_time
        duration
        createdAt
        updatedAt
      }
      startTime
      endTime
      rating
      notes
      overtimePenalty
      createdAt
      updatedAt
    }
  }
`;
export const createUser = /* GraphQL */ `
  mutation CreateUser(
    $input: CreateUserInput!
    $condition: ModelUserConditionInput
  ) {
    createUser(input: $input, condition: $condition) {
      id
      username
      email
      userGroup
      intakeFormDetails {
        personalDetails {
          fullName
          ageGroup
          height
          weight
          sex
          phoneNumber
          email
          emergencyContactName
          emergencyContactNumber
        }
        exerciseHistory {
          exerciseImportance
          weeklyExercise
          ageOfMostConsistency
          athleticismLevel
          exercisePrograms
          wantedExerciseAchievements
          confidenceToAchieveGoals
        }
        medicalHistory {
          hasMedicalConditions {
            isQuestionTrue
            reason
          }
          toldToLimitActivity {
            isQuestionTrue
            reason
          }
          injuries {
            isQuestionTrue
            reason
          }
          surgeries {
            isQuestionTrue
            reason
          }
          chronicDisease {
            isQuestionTrue
            reason
          }
          medications {
            isQuestionTrue
            reason
          }
        }
        behavioralQuestions {
          barrierToExercise
          confidenceToOvercomeBarrier
          friendAndFamilySupport
          exerciseImportance
          motivation
          resultsExpectation
        }
        trainerPreferences {
          language
          trainerSex
          coachPersonality
        }
      }
      profileInformation {
        firstName
        lastName
        emailAddress
        contactNumber
        coachRate
        cancellationRate
        about
        languages
        skills
        coachSchedule {
          scheduleExceptions {
            id
            type
            day
            coachRate
            startTime
            endTime
          }
        }
        timezone
        numOfVirtualClients
        socialMediaLinks
        profilePicture
        certifications
      }
      currentMembershipOption {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      pastMembershipOptions {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      averageRating
      coachReviews {
        id
        clientID
        client {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        coachID
        coach {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        title
        rating
        reviewText
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const updateUser = /* GraphQL */ `
  mutation UpdateUser(
    $input: UpdateUserInput!
    $condition: ModelUserConditionInput
  ) {
    updateUser(input: $input, condition: $condition) {
      id
      username
      email
      userGroup
      intakeFormDetails {
        personalDetails {
          fullName
          ageGroup
          height
          weight
          sex
          phoneNumber
          email
          emergencyContactName
          emergencyContactNumber
        }
        exerciseHistory {
          exerciseImportance
          weeklyExercise
          ageOfMostConsistency
          athleticismLevel
          exercisePrograms
          wantedExerciseAchievements
          confidenceToAchieveGoals
        }
        medicalHistory {
          hasMedicalConditions {
            isQuestionTrue
            reason
          }
          toldToLimitActivity {
            isQuestionTrue
            reason
          }
          injuries {
            isQuestionTrue
            reason
          }
          surgeries {
            isQuestionTrue
            reason
          }
          chronicDisease {
            isQuestionTrue
            reason
          }
          medications {
            isQuestionTrue
            reason
          }
        }
        behavioralQuestions {
          barrierToExercise
          confidenceToOvercomeBarrier
          friendAndFamilySupport
          exerciseImportance
          motivation
          resultsExpectation
        }
        trainerPreferences {
          language
          trainerSex
          coachPersonality
        }
      }
      profileInformation {
        firstName
        lastName
        emailAddress
        contactNumber
        coachRate
        cancellationRate
        about
        languages
        skills
        coachSchedule {
          scheduleExceptions {
            id
            type
            day
            coachRate
            startTime
            endTime
          }
        }
        timezone
        numOfVirtualClients
        socialMediaLinks
        profilePicture
        certifications
      }
      currentMembershipOption {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      pastMembershipOptions {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      averageRating
      coachReviews {
        id
        clientID
        client {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        coachID
        coach {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        title
        rating
        reviewText
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const deleteUser = /* GraphQL */ `
  mutation DeleteUser(
    $input: DeleteUserInput!
    $condition: ModelUserConditionInput
  ) {
    deleteUser(input: $input, condition: $condition) {
      id
      username
      email
      userGroup
      intakeFormDetails {
        personalDetails {
          fullName
          ageGroup
          height
          weight
          sex
          phoneNumber
          email
          emergencyContactName
          emergencyContactNumber
        }
        exerciseHistory {
          exerciseImportance
          weeklyExercise
          ageOfMostConsistency
          athleticismLevel
          exercisePrograms
          wantedExerciseAchievements
          confidenceToAchieveGoals
        }
        medicalHistory {
          hasMedicalConditions {
            isQuestionTrue
            reason
          }
          toldToLimitActivity {
            isQuestionTrue
            reason
          }
          injuries {
            isQuestionTrue
            reason
          }
          surgeries {
            isQuestionTrue
            reason
          }
          chronicDisease {
            isQuestionTrue
            reason
          }
          medications {
            isQuestionTrue
            reason
          }
        }
        behavioralQuestions {
          barrierToExercise
          confidenceToOvercomeBarrier
          friendAndFamilySupport
          exerciseImportance
          motivation
          resultsExpectation
        }
        trainerPreferences {
          language
          trainerSex
          coachPersonality
        }
      }
      profileInformation {
        firstName
        lastName
        emailAddress
        contactNumber
        coachRate
        cancellationRate
        about
        languages
        skills
        coachSchedule {
          scheduleExceptions {
            id
            type
            day
            coachRate
            startTime
            endTime
          }
        }
        timezone
        numOfVirtualClients
        socialMediaLinks
        profilePicture
        certifications
      }
      currentMembershipOption {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      pastMembershipOptions {
        membershipOption {
          name
          pricing
          features
        }
        startDate
        endDate
      }
      averageRating
      coachReviews {
        id
        clientID
        client {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        coachID
        coach {
          id
          username
          email
          userGroup
          profileInformation {
            firstName
            lastName
            emailAddress
            contactNumber
            coachRate
            cancellationRate
            about
            languages
            skills
            timezone
            numOfVirtualClients
            socialMediaLinks
            profilePicture
            certifications
          }
          currentMembershipOption {
            startDate
            endDate
          }
          pastMembershipOptions {
            startDate
            endDate
          }
          averageRating
          coachReviews {
            id
            clientID
            coachID
            title
            rating
            reviewText
            createdAt
            updatedAt
          }
          createdAt
          updatedAt
        }
        title
        rating
        reviewText
        createdAt
        updatedAt
      }
      createdAt
      updatedAt
    }
  }
`;
export const createReview = /* GraphQL */ `
  mutation CreateReview(
    $input: CreateReviewInput!
    $condition: ModelReviewConditionInput
  ) {
    createReview(input: $input, condition: $condition) {
      id
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      title
      rating
      reviewText
      createdAt
      updatedAt
    }
  }
`;
export const updateReview = /* GraphQL */ `
  mutation UpdateReview(
    $input: UpdateReviewInput!
    $condition: ModelReviewConditionInput
  ) {
    updateReview(input: $input, condition: $condition) {
      id
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      title
      rating
      reviewText
      createdAt
      updatedAt
    }
  }
`;
export const deleteReview = /* GraphQL */ `
  mutation DeleteReview(
    $input: DeleteReviewInput!
    $condition: ModelReviewConditionInput
  ) {
    deleteReview(input: $input, condition: $condition) {
      id
      clientID
      client {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      coachID
      coach {
        id
        username
        email
        userGroup
        intakeFormDetails {
          personalDetails {
            fullName
            ageGroup
            height
            weight
            sex
            phoneNumber
            email
            emergencyContactName
            emergencyContactNumber
          }
          exerciseHistory {
            exerciseImportance
            weeklyExercise
            ageOfMostConsistency
            athleticismLevel
            exercisePrograms
            wantedExerciseAchievements
            confidenceToAchieveGoals
          }
          behavioralQuestions {
            barrierToExercise
            confidenceToOvercomeBarrier
            friendAndFamilySupport
            exerciseImportance
            motivation
            resultsExpectation
          }
          trainerPreferences {
            language
            trainerSex
            coachPersonality
          }
        }
        profileInformation {
          firstName
          lastName
          emailAddress
          contactNumber
          coachRate
          cancellationRate
          about
          languages
          skills
          timezone
          numOfVirtualClients
          socialMediaLinks
          profilePicture
          certifications
        }
        currentMembershipOption {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        pastMembershipOptions {
          membershipOption {
            name
            pricing
            features
          }
          startDate
          endDate
        }
        averageRating
        coachReviews {
          id
          clientID
          client {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          coachID
          coach {
            id
            username
            email
            userGroup
            averageRating
            createdAt
            updatedAt
          }
          title
          rating
          reviewText
          createdAt
          updatedAt
        }
        createdAt
        updatedAt
      }
      title
      rating
      reviewText
      createdAt
      updatedAt
    }
  }
`;
export const createZoomMeet = /* GraphQL */ `
  mutation CreateZoomMeet(
    $input: CreateZoomMeetInput!
    $condition: ModelZoomMeetConditionInput
  ) {
    createZoomMeet(input: $input, condition: $condition) {
      id
      start_url
      topic
      join_url
      password
      start_time
      duration
      createdAt
      updatedAt
    }
  }
`;
export const updateZoomMeet = /* GraphQL */ `
  mutation UpdateZoomMeet(
    $input: UpdateZoomMeetInput!
    $condition: ModelZoomMeetConditionInput
  ) {
    updateZoomMeet(input: $input, condition: $condition) {
      id
      start_url
      topic
      join_url
      password
      start_time
      duration
      createdAt
      updatedAt
    }
  }
`;
export const deleteZoomMeet = /* GraphQL */ `
  mutation DeleteZoomMeet(
    $input: DeleteZoomMeetInput!
    $condition: ModelZoomMeetConditionInput
  ) {
    deleteZoomMeet(input: $input, condition: $condition) {
      id
      start_url
      topic
      join_url
      password
      start_time
      duration
      createdAt
      updatedAt
    }
  }
`;
